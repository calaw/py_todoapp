from ._anvil_designer import AllTaskTemplateTemplate
from anvil import *
import anvil.tables as tables
import anvil.tables.query as q
from anvil.tables import app_tables
from ...TaskEdit import TaskEdit
from datetime import datetime

class AllTaskTemplate(AllTaskTemplateTemplate):
  def __init__(self, **properties):
    # Set Form properties and Data Bindings.
    self.init_components(**properties)

    # Any code you write here will run when the form opens.

  def check_box_1_change(self, **event_args):
    """This method is called when this checkbox is checked or unchecked"""
    if self.item['done']==True :
      self.item['completed_on']=datetime.now()
      self.refresh_data_bindings()
      self.remove_from_parent()
    else:
      self.item['completed_on']=None
      self.refresh_data_bindings()
      self.remove_from_parent()


    pass

  def link_2_click(self, **event_args):
    """This method is called when the link is clicked"""
    alert(TaskEdit(item=self.item, title="Edit Task Details", large=True))
    self.refresh_data_bindings()
    #pass

  def link_1_click(self, **event_args):
    """This method is called when the link is clicked"""
    if self.item['completed_on'] == None :
       
        alert(str(self.item['description']))

    else:
      current_time=self.item['completed_on'].strftime('%b %d %Y %I:%M%p')
      alert(str(self.item['description'])+"\n"+"completed on:\n"+current_time)
   

  def delete_click(self, **event_args):
    """This method is called when the link is clicked"""
    self.item.delete()
    self.remove_from_parent()
   


    

 



 

 





